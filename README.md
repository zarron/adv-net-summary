# Summary
## P4
### Purpose
P4 is a language close to hardware, which is designed to program switches. Like this, these gain a lot more flexibility, while they are still efficient. P4 can be compiled for software switches, but also to FPGA like targets.

### Structure
![alt text](assets/p4.png "P4 Structure")

#### Parser
- `MyParser()`
- Determines which headers should be extracted
- Implemented as a state machine

#### Match-Action Pipeline
- `MyVerifyChecksum()`
  - verifies checksum
- `MyIngress()`
  - a list of tables, actions, and match on table commands
- `MyEgress()`
  - like ingress, but executed after queueing
- `MyComputeChecksum()`
  - computes the new checksum, if fields have changed

#### Deparser
- `MyDeparser()`
- creates actual packets form the data
- Adds headers back

### Metadata
Metadata is the data attached to a packet, and can be accessed the whole time, the packet is in the switch. It is split into 3 categories:

#### Standard Metadata
These are the fields, that exist per default for every packet, examples are:
- ingress port
- egress port

#### Custom Metadata
Fields that the programmer can define himself.

#### Headers
A structure containing multiple protocol headers with their fields.

### Control Plane Interactions

#### Tables
Tables are structures on the switch, that can be populated by the Control Plane. Their structure looks like this:

| action_name | match_1        | ... | action_param_1    | ... |
| ----------- | -------------- | --- | ----------------- | --- |
| match_ip    | 192.168.1.0/24 | ... | 9c:fc:e8:f1:36:23 | ... |
| drop        | 0.0.0.0/0      | ... |                   | ... |

Interaction methods with tables include `apply()`, `apply().hit` and `apply().action_run`

#### Registers
Arrays of variables, read and writeable by Control Plane and Switch

#### Counters
Arrays of counter variables, can only be incremented by switch, not read. Control Plane can read and reset.
They can also be attached to a table, to count matches for each entry.

#### Meters
Arrays of variables, that measure a rate of a certain process, can also be attached to a table. They can be configured by the control plane. They can be in a green, orange and red range. The switch can only read the color code.

## MPLS
MPLS is a forwarding technique, that can be used in internal networks, e.g. in a AS, where the topology is known. On top of the IP header of each packet, it assigns an MPLS header, with a label. Packets are then forwarded according this label.

### Terminology

- **FEC**: Forwarding Equivalence Classes; A group of IP packets that are forwarded in the same
- **LDP**: Label Distribution Protocol
- **LER**: Label Edge Router: Inserts labels on packets sent through backbone
- **LSP**: Label Switched Path
- **LSR**: Label-Switching Router: Packet forwarding based only on labels
- **RSVP-TE**: Resource Reservation Protocol-Traffic Engineering

### Stack based
The ingress router of the AS adds a stack of MPLS labels to the packet, which get popped one after another at each router, and decide its forwarding decision.

This means smart ingress router, dumb inner routers
### Swap based
The ingress router just adds one label, and each router reads the label, does its forwarding decision according to it and then swaps the label with one for the next router.

This means equally smart routers.

### LDP
A protocol to create/install LSPs. It results in the shortest path. There are two modes of installing the paths:

1. **Ordered Label Distribution Control mode:** </br>
A donwnstream LSR only assigns a label to a FEC if
    - it is egress for that FEC; *or*
    - it has received a label binding for that FEC on shortest path
2. **Independent Label Distribution Control mode:** </br>
A downstream LSR binds a label to any FEC it learns in the IGP independently of
any router.

### RSVP-TE
A protocol, that establishes unidirectional LSP based on the RSVP protocol. It uses also PATH messages from src to dst, and RESV from dst to src to install a reservation. It uses MPLS labels.

There is also an extension, where the src can specify the path to take.
## Traffic Engineering

Traffic Engineering are multiple techniques to enable load balancing to utilize all links, maintaining QoS, minimize packet loss, etc.

### ECMP
Equal cost multi path is used inside an OSPF network, to split traffic over multiple paths with the same weight. There are different methods to assign packets to paths. Usually, one tries to keep TCP flows together, and hashes over src port/ip, and dst port/ip.

### CONGA: Distributed Congestion-Aware Load Balancing for Datacenters

<p style="text-align: center;"> <img src="assets/conga.png" alt="CONGA"> </p>

* The main thesis: Datacenter load balancing is best done in the network
instead of the transport layer
* Requires global congestion-awareness to handle asymmetry.

CONGA leverages an existing datacenter overlay to implement a
leaf-to-leaf feedback loop and can be deployed without any modifications to the TCP stack.

### Flowlets
Like ECMP, but not whole TCP flows are separated, but Flowlets, "packet bursts" of a single TCP flow. This is also save from reordering.

- Flowlets change size based on congestion on the path
  - Uncongested path --> larger flowlets
  - Congested path --> smaller flowlets

==> Flowlet sizes implicitly encode path congestion information

## QOS

![alt text](assets/QoS.png "P4 Structure")

### Classification
Distinguish different classes of traffic

### Shaping/Policing
Limit average or/and maximum traffic rate.

#### Token Bucket
The token bucket is a concept to rate limit traffic. A bucket fills with a constant rate of tokens. The token bucket has a max size. Sending of packets needs tokens, and there is a maximum peek token usage rate.

#### Enforcing traffic specifications
If traffic exceeds specifications, there are three possibilities:
1. DROP
2. DELAY
3. MARK (mark packets that exceed specifications therefore lower their priority in the network)

### Buffer Acceptance
Decide if a packet should be dropped in advance, for example, to trigger rate limitation of TCP

There are two possible ways:
1. **Tail drop**: Drop packages when queue is full.
    - Simple to implement
    - Tends to synchronize TCP flows
2. **Random Early Detection (RED)**: Drop each arriving package with
   probability along the following curve:
    - <p style="text-align: center;"> <img src="assets/Random_early_detection.png" width=350> </p>
    - More fair, no synchronization of TCP flows
    - Harder to implement


### Scheduling/Queuing Strategy
Decides how outgoing packets are queued, and which queues are to be dequeued first.

|                       | Image                                                                                                                 | Idea                                                                                | Pros                                                         | Cons                                        |
| --------------------- | --------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------- |
| FIFO                  | <img src="https://gitlab.ethz.ch/nsg/public/adv-net-2020/-/raw/master/05-Traffic_control/images/fifo.png" width=800 > | First in first out                                                                  | Simple, no starvation                                        | No priorization, no fairness                |
| Priority Queuing (PQ) | <img src="https://gitlab.ethz.ch/nsg/public/adv-net-2020/-/raw/master/05-Traffic_control/images/pfifo.png" >          | Different classes have different queues. Queues have strict priorities              | simple, isolate high priority traffic, allow differentiation | Starvation of low priority traffic possible |
| Fair Queuing (FQ)     | <img src="https://gitlab.ethz.ch/nsg/public/adv-net-2020/-/raw/master/05-Traffic_control/images/sfq.png" >            | Different classes have different queues. They get dequeued in a round robin manner. | Provide isolation and fairness                               | Ressource intensive (one queue per flow)    |
| Weighted Fair Queuing | <img src="https://gitlab.ethz.ch/nsg/public/adv-net-2020/-/raw/master/05-Traffic_control/images/sfq.png" >            | Same as FQ, but dequeuing in weighted manner                                        | same as FQ, more differentiation                             | even harder to implement                    |


## VPN
- Idea: Interconnect geographically distributed sites in a private network
- Goals
  1. Support multiple customers
  2. Provide QoS guarantees
  3. Easy to use and manage

There are two approaches: User-managed and Provider-managed.

### User Managed VPN

#### Leased Lines (LL)
Leased Line is a dedicated connection between sites.

- Pros: Quality is very good
- Cons:
  - Expensive: Number of LLs is high for full mesh (O(n²))
  - Not flexible: Adding/removing leased lines is expensive and takes time

#### IP-Based VPN
Use IP tunnels using deticated protocols (Wireguard, IPSEC)

- Pros:
  - Each router only requires one interface to connect to all the others
- Cons:
  - Total number of tunnels is high (O(n²))
  - Adding new sites requires to modify many configurations
  - No QoS, only best effort
  - Security depends on used protocol and can be weak

### Provider Managed VPN
The customer is agnostic to the service.

#### Terminology

<p style="text-align: center;"> <img src="assets/vpn_terminology.png" alt="VPN" width=400> </p>

- **Customer Edge (CE)** : Sends packets through backbone and does not know any
  details of backbone.
- **Provider Edge (PE)** : maintains per-VPN configuration and does routing to
  other PE.
- **Provider \(P\)** : is within backbone, does not know anything about VPN.

#### Routing

- PE routers need to have routing table for each VPN it is attached to ("VRF",
  *VPN Routing and Forwarding Table*)
- CE advertises local routes to PE.
- PE advertises remote VPN routes to CE.
- Two problems:
  1. Different VPNs can use overlapping IP space, how do PEs distinguish them?
      - Ensure uniqueness of addresses by prepending them with Route
	Distinguisher (AS number or IP address)
  2. How to ensure that CEs only learn the route of own VPNs?
     - Assign unique label (Route Target) to each VPN and have PE attach the tag to the BGP
     route before propagating it.

#### Forwarding
- CE router send pure IP packets
- PE router encapsulate the IP packets with MPLS labels
- Each PE pushes two labels:
  1. outer label to identify next-hop PE
  2. inner label identifying VRF to use in remote PE (distributed by BGP)


## Fast Convergence

### Introduction

Definition
> Routing convergence is the transition from a routing and forwarding state to
> another state.

- Sudden or planned
- distributed process --> inconsistency may happen
- Problems:
  - Traffic losses (blackholing)
  - Forwarding loops
- Goal: Time of convergence should be less than 50ms.

### Fast Convergence in IP networks

#### Fast Detection

Goals:
- Fast detection
- High accuracy
- Low overhead

There are three mechanisms:
1. Rely on physical layer:
  - optical layers can detect failures through loss of light.
  - Pros:
    - As fast as possible
    - No overhead
  - Cons:
    - does not work for all types of links
    - Does not detect all kind of failures (i.e. if a switch is in between)
2. Rely on "hellos" / "keepalives" / ... (protocol intern)
  - Adjacent routers exchange "hellos" and signal a failure if $`k`$ of them
    are missed in a row.
  - Each protocol comes with own "hello"-protocol.
  - Pros:
    - Works on any router
    - Detects wider range of failures
  - Cons:
    - Slow
    - Huge overhead
3. Bidirectional Forwarding Detection (BFD)
  - Protocol-agnostic hello service
  - runs in hardware --> allows to be more aggressive!
  - Configure other protocols (BGP, OSPF, ...) to not send any "hellos" and
    use BFD instead
  - Pros:
    - Fast detection
    - Low overhead
    - High coverage
  - Cons:
    - Not all router can run BFD in hardware

Priorities of use:
1. Link-layer mechanisms
2. BFD
3. protocol-based detection as a fallback

#### Fast Propagation

How to quickly communicate the failure network wide?

Ensure two things:
1. Immediate flooding of failure notifications
2. Sending flooded package with priority

#### Fast Computation

How to quickly recompute forwarding table?

- No problem for shortest-path-based protocols (e.g. OSPF), since computation
  of spanning tree takes a few milliseconds
- For BGP, this is a problem since computation is done per prefix!
  - Solution: Force routers to advertise all external routes even if thy are not chosen as
    best.

#### Fast Updates
Updating the Forwarding Information Base (*FIB*) is the bottleneck of
convergence process as it is O(#prefixes).

**Long term solution:**
Reorganize FIB such that it allows fast updates.
1. Pre-compute backup state
2. Pre-load backup state
3. Activate backup state upon failure detection

##### Loop-free alternative (LFA) for IGP
Definition:
> A neighboring router N is a LFA for a router S to destination D if:
>
> dist(N,D) < dist(N, S) + dist(S, D)

Computation:
```
For all links (X, R):
  For all direct neighbors (X, N):
    Compute shortest path tree of N
      If (X,R) not in Shortest path tree of N:
	Add (X, R) as canditate LFA for all destinations
```

**Remote LFAs**
<p style="text-align: center;"> <img src="assets/remote_LFA.png" alt="Remote
LFA" width=500> </p>

Computation:
```
On router X:
  For all destinations Y:
    nh = nexthop(from=X, to=Y)
    P = set of nodes that X can reach without traversing (X, nh)
    Q = set of nodes that can reach Y without traversing (X, nh)
    candidates_RLFA = P∩Q
```

##### Prefix-independent convergence (PIC)

###### Problem:
If the FiB is *flattend*, this means the prefixes are directly mapped to an interface, in the worst case every entry must be updates.
<p style="text-align: center;"> <img src="assets/pic_problem.png" alt="PIC
Problem"> </p>

###### Goal:
Do not update all prefixes one-by-one.

###### Solution:
Maintain the hierarchy between BGP next-hops and IGP next-hops in the
FIB as well.

<p style="text-align: center;"> <img src="assets/pic_solution.png" alt="PIC
solution"> </p>



### Fast Convergence in MPLS networks

Principles:
- Pre-establish secondary LSP
- Switch to using secondary LSP upon failure detection

#### End-to-end LSP protection

When a a failure happens, the adjacent router sends a `PathErr` message to the
ingress to trigger the switch.

<p style="text-align: center;"> <img src="assets/end-to-end-lsp-protection.png" alt=" End-to-end LSP protection"> </p>

- Pros:
  - Immediate activation possible
  - No coordination
- Cons:
  - One has to establish secondary LSP. --> Memory overhead doubled.
  - `PathErr` must travel to all ingress routers. This is slow.

#### Local LSP protection

Each LSR will signal a protection LSP to cover failure of used link.

<p style="text-align: center;"> <img src="assets/local_LSP_protection.png" alt="Local LSP protection"> </p>

- Pros:
  - Immediate switch after detection possible
- Cons:
  - A large number of LSPs might needed.

## Mulitcast

### Cast Types

|                                                                   |                                                                  |
| --------------------------------------------------                | -------------------------------------------------                |
| <img src="assets/unicast.png" width=200> </br> normal application | <img src="assets/multicast.png" width=200> </br> TV broadcasting |
| <img src="assets/anycst.png" width=200> </br> e.g. CDN, DNS etc.  | <img src="assets/broadcast.png" width=200> </br> reach everybody |

### Principle
One package is sent from the sender, and gets duplicated at routers if needed.

Routers retransmit each packet such that:
- packet reach all receivers
- packets traverse each link only once.

### Addressing
A subset of IP space is reserved for multicast:

- `224.0.0.0-239.255.255.255` is used as multicast addresses.
- `224.0.0.1` All hosts
- `224.0.0.2` All multicast routers
- `224.0.0.5` All OSFP routers
- `239.0.0.0-239.255.255.255` can only be used within an AS.



### Receiving
IP addresses are mapped to ethernet addresses with a specific multicast prefix. Host can be configured to listen to this additional logical MAC address as well.

### Registering
The hosts use the *Internet Group Management Protocol* IGMP to register to a mulitcast address. Adjacent routers keep track of the registrations.

### Routing

#### Pro Active
Group membership information is flooded from to the whole network. Every router calculates the shortest path form every source to every registered host, and decides accordingly if it has to forward/duplicate packages.

##### Pros:
+ All routers have full knowledge -> optimal solution

##### Cons:
- Huge overhead due to flooding
- every router must maintain state for every possible reservation
- computation is $`O(\text{\#sources} \cdot \text{\#group} )`$, i.e.  it does not scale well with many sources.

#### Reactive - Flood and Prune

##### Reverse Path Filtering (RPF) Algorithm
Router only forward/broadcast packets if they came directly from the sender, i.e. they arrived on the interface which is part of the shortest path from this router to the source.

###### Sender Based:
The intermediate router only forwards a packet to a second intermediate router if the shortest path from the sender to the second intermediate router includes the first intermediate router.

###### Receiver Based
The router floods packets to all interfaces, except it receives a prune message on an interface. This is widely used.


#### Reactive - Rendezvous Points
A single router is specified as rendezvous point. It's address is well known. Every leaf router that wants to receive the multicast traffic sends a `join` message to the rendezvous router, and every intermediate router adds the interface on which it's received the `join` message to its broadcast list. Like this every packet from the rendezvous router to the receivers is sent only once over every necessary link.

### Protocol Independent Multicast - PIM
The mostly deployed multicast protocol. Uses Reactive Mode, usually with Rendezvous Points.

It has two modes:
- **Dense mode:** Flood and prune
- **Sparse mode:** Rendezvous Points
