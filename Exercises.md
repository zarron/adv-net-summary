## Tutorials

## MPLS

### Label encoding
```
Label: Label Value, 20 bits
Exp: Experimental Use, 3 bits
S: Bottom of Stack, 1 bit
TTL: Time to Live, 8 bits
```

### Parser
```
state parse_ethernet {
  packet.extract(hdr.ethernet);
  transition select(hdr.ethernet.etherType) {
    TYPE_MPLS: parse_mpls;
    TYPE_IPV4: parse_ipv4;
    default: accept;
  }
}

state parse_mpls {
  packet.extract(hdr.mpls.next);
  transition select(hdr.mpls.last.s) {
    1: parse_ipv4;
    default: parse_mpls;
  }
}

state parse_ipv4 {
  packet.extract(hdr.ipv4);
  transition accept;
}
```


## RSVP

Match on both, `source` and `destination` since we want to establish paths
pairwise.

## Load balancing

### ECMP

#### Data plane

- Compute hash over flow `(src ip , dst ip, src port, dst port, protocol)`
- Store hash in `metadata`.

```
if(dst == neighbor):
  ip_forward(packet)
else:
  h = hash(flow)
  g = group(flow)

  add_mpls_lable(h, g)

  mpls_forward(packet)
```

#### Control plane
```python
for all (src, dst) in switches:
  paths = compute_equal_cost_shortest_paths(src, dst)

  # install table entries

  # Connect hosts to switches
  if(src == dst):
    for host in hosts(src):
      install_forwarding_rule(src, host)

  # Direct forward via MPLS
  else if(len(paths) == 1 and):
    for host in hosts(dst):
      install_next_hop_rule(src, host)

  # Forward via ECMP
  else:
    for host in host(dst):
      add_ecmp_group(host)
```

### Flowlet
Enhancement of ECMP: Re-hash after 50ms have passed between two packets.

#### Data plane
- 2 Registers:
  - `flowlet_to_id`: keeps random id of flowlet, this is added to the hash.
  Same `flowlet_to_id` --> same hash --> same forwarding
  - `flowlet_time_stamp`: Keep time stamp for the last observed packet of a
  flow.
- Registers are accessible by hash(flow)
- If `in_time - flowlet_time_stamp > Δ`: update `flowlet_to_id`. The packet
  will take another path then one of the same flow before.

#### Control plane

Same as in ECMP

## Traffic control

### Common Traffic Control Solutions

| Scenario                                             | Solution                                          |
| ---                                                  | ---                                               |
| Limit total bandwidth to a known rate                | TBF, HTB with child classes                       |
| Limit bandwidth of a particular service              | HTB classes and classifying with a filter traffic |
| Reserve bandwidth for application                    | HTB with children classes and classifying         |
| Prefer latency sensitive traffic                     | PRIO inside an HTB class                          |
| Allow equitable distribution of unreserved bandwidth | HTB with borrowing                                |
| Ensure that a particular type of traffic is dropped  | Policer attached to a filter with a drop action   |

- TBF: Token Bucket Filter
- HTB: Hierarchical Token Bucket


## BGP Free Core & BGP VPN over MPLS

Challenges:
- Reachability: Access `10.0.0.0/8` within company
- Isolation: Make `10.0.0.0/8` unaccessible for other companies
- Scalability: Do not overload ISP and use MPLS

### Steps
1. Configure BGP and a BGP Free Core
    1. Configure the iBGP sessions
    1. Configure the eBGP sessions
    1. Configure MPLS forwarding with LDP
1. Configure the VPN over BGP using VRF
    1. Configure eBGP in a VRF
    1. Configure the VPNs

## Fast reroute

### Control-plane

- For each switch and link, compute an LFA next hop to which the switch can
  fall back if the link fails.
- Install this LFA in the switches.

### Data-plane

- Store the alternative next hop on the switch.
- Read the link state for the primary next hop, and select the alternative next
  hop if the primary is down such that all traffic is immediately rerouted.

## IP multicast

### PIM

PIM is used between routers to establish multicast routing trees.

1. Activate PIM on all routers
2. Configure Rendez-vous point on all routers

Let the magic happen! See
[this video](https://www.youtube.com/watch?v=PhzMcUcS6UA) for an explanation.

### IGMP

1. Activate on all host-facing interfaces
2. Join a group via command line interface
